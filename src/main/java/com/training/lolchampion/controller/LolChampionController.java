package com.training.lolchampion.controller;

import com.training.lolchampion.entities.LolChampion;
import com.training.lolchampion.service.LolChampionService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import java.util.List;
import java.util.NoSuchElementException;


@RestController
@RequestMapping("/api/v1/lolchampion")
public class LolChampionController {

    private static final Logger LOG = LoggerFactory.getLogger(LolChampionController.class);

    @Autowired
    private LolChampionService lolChampionService;

    @GetMapping
    public List<LolChampion> findAll() {
        return lolChampionService.findAll();
    }

    @PostMapping
    public ResponseEntity<LolChampion> save(@RequestBody LolChampion lolChampion) {
        LOG.debug("Request to create lolchampion [" +lolChampion + "]");
        try {
            return new ResponseEntity<LolChampion>(lolChampionService.save(lolChampion), HttpStatus.CREATED);
        } catch (NoSuchElementException ex) {
            //return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}


